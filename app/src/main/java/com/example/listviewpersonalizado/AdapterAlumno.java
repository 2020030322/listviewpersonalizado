package com.example.listviewpersonalizado;

import android.app.Activity;
import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class AdapterAlumno extends ArrayAdapter<AlumnoItem>{
    int grupo;
    Activity Context;
    ArrayList<AlumnoItem> list;
    LayoutInflater inflater;

    public AdapterAlumno(Activity context,int grupo,int ID, ArrayList<AlumnoItem> list) {
        super(context, ID, list);
        this.grupo = grupo;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        this.list = list;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View itemView = inflater.inflate(grupo,viewGroup,false);
        ImageView imagen = (ImageView) itemView.findViewById(R.id.imgAlumno);
        imagen.setImageResource(list.get(i).getImageAlum());
        TextView textCategoria = (TextView) itemView.findViewById(R.id.lblMatriculaAlumno);
        textCategoria.setText(list.get(i).getMatriculaAlum());
        TextView textDescripcion = (TextView) itemView.findViewById(R.id.lblNombreAlumno);
        textDescripcion.setText(list.get(i).getNombreAlum());
        return itemView;
    }

    @Override
    public View getDropDownView(int i,View view,ViewGroup viewGroup) {
        return getView(i,view,viewGroup);
    }
}
