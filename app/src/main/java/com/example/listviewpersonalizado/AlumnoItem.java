package com.example.listviewpersonalizado;

public class AlumnoItem {
    private String matriculaAlum;
    private String nombreAlum;
    private Integer imageAlum;

//PARAMETROS
    public AlumnoItem(String text1, String text2, Integer imgAlum){
        this.matriculaAlum = text1;
        this.nombreAlum = text2;
        this.imageAlum = imgAlum;
    }
//METODOS GETTER & SETTER
    public String getMatriculaAlum() {
        return matriculaAlum;
    }

    public void setMatriculaAlum(String matriculaAlum) {
        this.matriculaAlum = matriculaAlum;
    }

    public String getNombreAlum() {
        return nombreAlum;
    }

    public void setNombreAlum(String nombreAlum) {
        this.nombreAlum = nombreAlum;
    }

    public Integer getImageAlum() {
        return imageAlum;
    }

    public void setImageAlum(Integer imageAlum) {
        this.imageAlum = imageAlum;
    }
}
