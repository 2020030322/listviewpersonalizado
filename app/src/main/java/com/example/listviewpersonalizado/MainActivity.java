package com.example.listviewpersonalizado;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    ListView sp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayList<AlumnoItem> list = new ArrayList<>();
        list.add(new AlumnoItem(getString(R.string.itemAlumno1), getString(R.string.msgAlumno1), R.drawable.img2019030344));
        list.add(new AlumnoItem(getString(R.string.itemAlumno2), getString(R.string.msgAlumno2), R.drawable.img2020030174));
        list.add(new AlumnoItem(getString(R.string.itemAlumno3), getString(R.string.msgAlumno3), R.drawable.img2020030176));
        list.add(new AlumnoItem(getString(R.string.itemAlumno4), getString(R.string.msgAlumno4), R.drawable.img2020030181));
        list.add(new AlumnoItem(getString(R.string.itemAlumno5), getString(R.string.msgAlumno5), R.drawable.img2020030184));
        list.add(new AlumnoItem(getString(R.string.itemAlumno6), getString(R.string.msgAlumno6), R.drawable.img2020030189));
        list.add(new AlumnoItem(getString(R.string.itemAlumno7), getString(R.string.msgAlumno7), R.drawable.img2020030199));
        list.add(new AlumnoItem(getString(R.string.itemAlumno8), getString(R.string.msgAlumno8), R.drawable.img2020030212));
        list.add(new AlumnoItem(getString(R.string.itemAlumno9), getString(R.string.msgAlumno9), R.drawable.img2020030241));
        list.add(new AlumnoItem(getString(R.string.itemAlumno10), getString(R.string.msgAlumno10), R.drawable.img2020030243));

        list.add(new AlumnoItem(getString(R.string.itemAlumno11), getString(R.string.msgAlumno11), R.drawable.img2020030249));
        list.add(new AlumnoItem(getString(R.string.itemAlumno12), getString(R.string.msgAlumno12), R.drawable.img2020030264));
        list.add(new AlumnoItem(getString(R.string.itemAlumno13), getString(R.string.msgAlumno13), R.drawable.img2020030268));
        list.add(new AlumnoItem(getString(R.string.itemAlumno14), getString(R.string.msgAlumno14), R.drawable.img2020030292));
        list.add(new AlumnoItem(getString(R.string.itemAlumno15), getString(R.string.msgAlumno15), R.drawable.img2020030304));
        list.add(new AlumnoItem(getString(R.string.itemAlumno16), getString(R.string.msgAlumno16), R.drawable.img2020030306));
        list.add(new AlumnoItem(getString(R.string.itemAlumno17), getString(R.string.msgAlumno17), R.drawable.img2020030313));
        list.add(new AlumnoItem(getString(R.string.itemAlumno18), getString(R.string.msgAlumno18), R.drawable.img2020030315));
        list.add(new AlumnoItem(getString(R.string.itemAlumno19), getString(R.string.msgAlumno19), R.drawable.img2020030322));
        list.add(new AlumnoItem(getString(R.string.itemAlumno20), getString(R.string.msgAlumno20), R.drawable.img2020030325));

        list.add(new AlumnoItem(getString(R.string.itemAlumno21), getString(R.string.msgAlumno21), R.drawable.img2020030327));
        list.add(new AlumnoItem(getString(R.string.itemAlumno22), getString(R.string.msgAlumno22), R.drawable.img2020030329));
        list.add(new AlumnoItem(getString(R.string.itemAlumno23), getString(R.string.msgAlumno23), R.drawable.img2020030332));
        list.add(new AlumnoItem(getString(R.string.itemAlumno24), getString(R.string.msgAlumno24), R.drawable.img2020030333));
        list.add(new AlumnoItem(getString(R.string.itemAlumno25), getString(R.string.msgAlumno25), R.drawable.img2020030389));
        list.add(new AlumnoItem(getString(R.string.itemAlumno26), getString(R.string.msgAlumno26), R.drawable.img2020030766));
        list.add(new AlumnoItem(getString(R.string.itemAlumno27), getString(R.string.msgAlumno27), R.drawable.img2020030771));
        list.add(new AlumnoItem(getString(R.string.itemAlumno28), getString(R.string.msgAlumno28), R.drawable.img2020030777));
        list.add(new AlumnoItem(getString(R.string.itemAlumno29), getString(R.string.msgAlumno29), R.drawable.img2020030799));
        list.add(new AlumnoItem(getString(R.string.itemAlumno30), getString(R.string.msgAlumno30), R.drawable.img2020030808));
        list.add(new AlumnoItem(getString(R.string.itemAlumno31), getString(R.string.msgAlumno31), R.drawable.img2020030819));
        list.add(new AlumnoItem(getString(R.string.itemAlumno32), getString(R.string.msgAlumno32), R.drawable.img2020030865));

        sp = (ListView) findViewById(R.id.LVP);
        AdapterAlumno adapter = new AdapterAlumno(this,R.layout.layout_itemalumno,R.id.lblMatriculaAlumno,list);
        sp.setAdapter(adapter);
        sp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(view.getContext(),getString(R.string.msgSeleccionado).toString()+": "+((AlumnoItem) adapterView.getItemAtPosition(i)).getNombreAlum(),Toast.LENGTH_SHORT).show();
            }
        });
    }
}